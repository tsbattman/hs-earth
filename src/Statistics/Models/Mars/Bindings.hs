{-# LANGUAGE ForeignFunctionInterface #-}

module Statistics.Models.Mars.Bindings (
    c'fitEarth
  , c'evalSubsetsXtx
  , c'freeEarth
  , c'predictEarth
  ) where

import Foreign.C (CBool(..), CDouble(..), CInt(..), CString)
import Foreign.Ptr (Ptr)

foreign import ccall "Earth"
  c'fitEarth ::
       Ptr CDouble -- out: GCV of the best model i.e. BestSet columns of bx
    -> Ptr CInt    -- out: max term nbr in final model, after removing lin dep terms
    -> Ptr CInt    -- out: reason we terminated the foward pass
    -> Ptr CBool   -- out: nMaxTerms x 1, indices of best set of cols of bx
    -> Ptr CDouble -- out: nCases x nMaxTerms
    -> Ptr CInt    -- out: nMaxTerms x nPreds, -1,0,1,2 for iTerm, iPred
    -> Ptr CDouble -- out: nMaxTerms x nPreds, cut for iTerm, iPred
    -> Ptr CDouble -- out: nCases x nResp
    -> Ptr CDouble -- out: nMaxTerms x nResp
    -> Ptr CDouble -- in: nCases x nPreds
    -> Ptr CDouble -- in: nCases x nResp
    -> Ptr CDouble -- in: nCases x 1, can be NULL, not yet supported
    -> Ptr CDouble -- in: number of rows in x and elements in y
    -> CInt        -- in: number of cols in y
    -> CInt        -- in: number of cols in x
    -> CInt        -- in: Friedman's mi
    -> CInt        -- in: includes the intercept term
    -> CDouble     -- in: GCV penalty per knot
    -> CDouble     -- in: forward step threshold
    -> CInt        -- in: set to non zero to override internal calculation
    -> CInt        -- in: set to non zero to override internal calculation
    -> CBool       -- in: do backward pass
    -> CInt        -- in: Fast MARS K
    -> CDouble     -- in: Fast MARS ageing coef
    -> CDouble     -- in: penalty for adding a new variable
    -> CInt        -- in: nPreds x 1, 1 if predictor must enter linearly
    -> CDouble     -- in: for adjusting endspan for interaction terms
    -> CBool       -- in: assume predictor linear if knot is max predictor value
    -> CBool       -- in: 1 to use the beta cache, for speed
    -> CDouble     -- in: 0 none 1 overview 2 forward 3 pruning 4 more pruning
    -> CString     -- in: predictor names in trace printfs
    -> IO ()

foreign import ccall "EvalSubsetsUsingXtxR"
  c'evalSubsetsXtx ::
       Ptr CDouble -- out: specifies which cols in bx are in best set
    -> Ptr CDouble -- out: nTerms x 1
    -> Ptr CInt    -- in
    -> Ptr CInt    -- in: number of cols in y
    -> Ptr CInt    -- in
    -> Ptr CDouble -- in: MARS basis matrix, all cols must be indep
    -> Ptr CDouble -- in: nCases * nResp (possibly weighted)
    -> Ptr CDouble -- in: pTrace
    -> IO ()

foreign import ccall "PredictEarth"
  c'predictEarth ::
       Ptr CDouble -- out: vector nResp
    -> Ptr CDouble -- in: vector nPreds x 1 of input values
    -> Ptr CBool   -- in: nMaxTerms x 1, indices of best set of cols of bx
    -> Ptr CInt    -- in: nMaxTerms x nPreds, -1,0,1,2 for iTerm, iPred
    -> Ptr CDouble -- in: nMaxTerms x nPreds, cut for iTerm, iPred
    -> Ptr CDouble -- in: nMaxTerms x nResp
    -> CInt        -- in: number of cols in x
    -> CInt        -- in: number of cols in y
    -> CInt        -- nTerms
    -> CInt        -- nMaxTerms
    -> IO ()

foreign import ccall "FreeEarth" c'freeEarth :: IO ()
